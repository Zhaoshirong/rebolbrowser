Rebol [
    notes: {Generates the download table and is called by replpad}
]

    
    
    do https://raw.githubusercontent.com/gchiu/rebol-misc/master/latest-of.reb
    
    replpad-write/html spaced [
    <table id="oss-selection" style="width:80%">
            <tr>
                {<th align="left"> Operating System </th> <th align="left"> Bits </th> <th align="left"> Rebol OS tuple </th>}
            </tr>
            <tr>
                {<td> Android Arm </td> <td> 32 </td> <td><a href="#child1" onclick="input_resolve('latest-of 0.13.2'); return false">0.13.2</a></td>}
            </tr>

            <tr>
                {<td> Windows </td> <td> 32 </td> <td><a href="#child1" onclick="input_resolve('latest-of 0.3.1'); return false">0.3.1</a></td>}
            </tr>

           <tr>
                {<td> Windows </td> <td> 64 </td> <td> <a href="#child1" onclick="input_resolve('latest-of 0.3.40'); return false">0.3.40</a> </td>}
            </tr>

            <tr>
                {<td> OSX </td> <td> 64 </td> <td> <a href="#child1" onclick="input_resolve('latest-of 0.2.40'); return false">0.2.40</a> </td>}
            </tr>
            <tr>
                {<td> Linux </td> <td> 32 </td> <td><a href="#child1" onclick="input_resolve('latest-of 0.4.4'); return false">0.4.4</a></td>}
            </tr>
            <tr>
                {<td> Linux </td> <td> 64 </td> <td><a href="#child1" onclick="input_resolve('latest-of 0.4.40'); return false">0.4.40</a> </td>}
            </tr>
        </table>
    ]

    replpad-write/html {<div class="note">Type eg: <b>latest-of 0.3.40</b> to get the latest builds, or, click on the OS tuple! link </div><br/>}

